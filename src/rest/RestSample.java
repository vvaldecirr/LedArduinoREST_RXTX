package rest;

//http://localhost:8080/LedArduinoREST_RXTX/method?param=teste

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import control.LoadLooper;

@Path("/method")
public class RestSample {
	
	@GET
	@Produces("text/plain")
	public String echo(@QueryParam("param") String param) {
		// Call melhod here:
		return "echo: " + param;
	}

}
