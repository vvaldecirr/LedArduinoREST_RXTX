package rest;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.IOUtils;

public class Post_JSON {

	public static void main(String[] args) {
		
		new Thread() {
			@Override
		    public void run() {
				
				String placa = "NAH8425";
				Double s1=0.0;		// speed Km/h
				Double s2=180.0;	// fuel L
				Double s3=0.0;		// temperature ºC
				
				String json = "";
				
				// car simulation
				while (s2 > 0.0) {
					String data = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS"));

					// increase speed
					if (s1!=160.0)
						s1+=1.5;
					// decrease fuel
//					if (s2<=10.0)
//						System.out.println("LOW TANK");
					s2-=1.9;
					// increase temperature
//					if (s3==110.0)
//						System.out.println("OVERHEAT");
//					else
						s3+=1.3;
						
					if (json.equals("")) {
						json += "{ \"placa\" : \""+placa+"\", \"s1\" : \""+Double.parseDouble(String.format("%.2f", s1))+"\", \"s2\" : \""+Double.parseDouble(String.format("%.2f", s2))+"\", \"s3\" : \""+Double.parseDouble(String.format("%.2f", s3))+"\", \"dataHora\" : \""+data+"\" }";
					} else {
						json += ", { \"placa\" : \""+placa+"\", \"s1\" : \""+Double.parseDouble(String.format("%.2f", s1))+"\", \"s2\" : \""+Double.parseDouble(String.format("%.2f", s2))+"\", \"s3\" : \""+Double.parseDouble(String.format("%.2f", s3))+"\", \"dataHora\" : \""+data+"\" }";
					}

					
//					try {
//						Thread.sleep(110);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}

				}

				Post_JSON.Post_JSON("["+json+"]");
//				System.out.println(json);
				
			}
		}.start();
		
		new Thread() {
			@Override
		    public void run() {
				
				String placa = "ODU4962";
				Double s1=0.0;		// speed Km/h
				Double s2=100.0;	// fuel L
				Double s3=0.0;		// temperature ºC
				
				String json = "";
				
				// car simulation
				while (s2 > 0.0) {
					String data = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS"));

					// increase speed
					if (s1!=160.0)
						s1+=1.5;
					// decrease fuel
//					if (s2<=10.0)
//						System.out.println("LOW TANK");
					s2-=1.9;
					// increase temperature
//					if (s3==110.0)
//						System.out.println("OVERHEAT");
//					else
						s3+=1.2;
					
						if (json.equals("")) {
							json += "{ \"placa\" : \""+placa+"\", \"s1\" : \""+Double.parseDouble(String.format("%.2f", s1))+"\", \"s2\" : \""+Double.parseDouble(String.format("%.2f", s2))+"\", \"s3\" : \""+Double.parseDouble(String.format("%.2f", s3))+"\", \"dataHora\" : \""+data+"\" }";
						} else {
							json += ", { \"placa\" : \""+placa+"\", \"s1\" : \""+Double.parseDouble(String.format("%.2f", s1))+"\", \"s2\" : \""+Double.parseDouble(String.format("%.2f", s2))+"\", \"s3\" : \""+Double.parseDouble(String.format("%.2f", s3))+"\", \"dataHora\" : \""+data+"\" }";
						}
					
//					try {
//						Thread.sleep(90);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
				}

				Post_JSON.Post_JSON("["+json+"]");
//				System.out.println(json);
				
			}
		}.start();
		
		new Thread() {
			@Override
		    public void run() {
				
				String placa = "NRF5481";
				Double s1=0.0;		// speed Km/h
				Double s2=150.0;	// fuel L
				Double s3=0.0;		// temperature ºC
				
				String json = "";
				
				// car simulation
				while (s2 > 0.0) {
					String data = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS"));

					// increase speed
					if (s1!=160.0)
						s1+=1.8;
					// decrease fuel
//					if (s2<=10.0)
//						System.out.println("LOW TANK");
					s2-=1.8;
					// increase temperature
//					if (s3==110.0)
//						System.out.println("OVERHEAT");
//					else
						s3+=1;
					
						if (json.equals("")) {
							json += "{ \"placa\" : \""+placa+"\", \"s1\" : \""+Double.parseDouble(String.format("%.2f", s1))+"\", \"s2\" : \""+Double.parseDouble(String.format("%.2f", s2))+"\", \"s3\" : \""+Double.parseDouble(String.format("%.2f", s3))+"\", \"dataHora\" : \""+data+"\" }";
						} else {
							json += ", { \"placa\" : \""+placa+"\", \"s1\" : \""+Double.parseDouble(String.format("%.2f", s1))+"\", \"s2\" : \""+Double.parseDouble(String.format("%.2f", s2))+"\", \"s3\" : \""+Double.parseDouble(String.format("%.2f", s3))+"\", \"dataHora\" : \""+data+"\" }";
						}
					
//					try {
//						Thread.sleep(100);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
				}

				Post_JSON.Post_JSON("["+json+"]");
//				System.out.println(json);
				
			}
		}.start();
		
		
	}

	public static void Post_JSON(String json) {

		String query_url = "http://localhost:8080/APIRestFul/rest/obd/enviarDados";
//		String json = "{ \"method\" : \"guru.test\", \"params\" : [ \"jinu awad\" ], \"id\" : 123 }";

		try {

			URL url = new URL(query_url);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(5000);
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");

			OutputStream os = conn.getOutputStream();
			os.write(json.getBytes("UTF-8"));
			os.close();

			// read the response
			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");

//			System.out.println(result);
//
//			System.out.println("result after Reading JSON Response");
//
//			JSONObject myResponse = new JSONObject(result);
//			System.out.println("jsonrpc- " + myResponse.getString("jsonrpc"));
//			System.out.println("id- " + myResponse.getInt("id"));
//			System.out.println("result- " + myResponse.getString("result"));

			in.close();
			conn.disconnect();

		} catch (Exception e) {
//			System.out.println(e);
			e.printStackTrace();
		}

	}

}
