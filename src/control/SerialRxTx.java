package control;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import model.SimpleData;

public class SerialRxTx implements SerialPortEventListener {

	private SerialPort		serialPort = null;
	private SimpleData		simpleData = new SimpleData();
	private String			appName;
	private BufferedReader	input;
	private OutputStream	output;
	private String serialPportName = "COM4";//"/dev/ttyACM0";
	
	private static final int TIME_OUT = 1000;
	private static final int DATA_RATE = 9600;
	
	public boolean iniciaSerial () {
		boolean status = false;
		try {
			// Obtém portas seriais do sistema
			//System.setProperty("gnu.io.rxtx.SerialPorts", this.serialPportName);
			
			CommPortIdentifier portId = null;
			Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
					
			while (portId == null && portEnum.hasMoreElements()) {
				CommPortIdentifier currentPortId = (CommPortIdentifier) portEnum.nextElement();
				
				if (currentPortId.getName().equals(serialPportName) || currentPortId.getName().startsWith(serialPportName)) {
					serialPort = (SerialPort) currentPortId.open(appName, TIME_OUT);
					portId = currentPortId;
					System.out.println("Conectado em: " + currentPortId.getName());
					break;
				}
			}
			
			if (portId == null || serialPort == null)
				return false;
			
			serialPort.setSerialPortParams(
					DATA_RATE,					// taxa de transferência da porta serial
					SerialPort.DATABITS_8,		// taxa de 10 bits 8 (envio)
					SerialPort.STOPBITS_1,		// taxa de 10 bits 1 (recebimento)
					SerialPort.PARITY_NONE);	// receber e enviar dados
			
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
			
			status = true;
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			status = false;
		}
		
		return status;
	}
	
	public void sendData (String data) {
		try {
			output = serialPort.getOutputStream();
			output.write(data.getBytes());
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}
	
	public synchronized void close () {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}
	
	/**
	 * Método que lida com a chegadas de dados pela porta serial do computador 
	 */
	@Override
	public void serialEvent(SerialPortEvent spe) {
		try {
			switch (spe.getEventType()) {
			case SerialPortEvent.DATA_AVAILABLE:
				if (input == null) {
					input = new BufferedReader(
								new InputStreamReader(serialPort.getInputStream())
							);
				}
				
				if (input.ready()) {
					simpleData.setLeituraComando(input.readLine());
					System.out.println(">" + simpleData.getLeituraComando());
				}
				break;

			default:
				break;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public SimpleData getProtocolo() {
		return simpleData;
	}

	public void setProtocolo(SimpleData simpleData) {
		this.simpleData = simpleData;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getSerialPportName() {
		return serialPportName;
	}

	public void setSerialPportName(String serialPportName) {
		this.serialPportName = serialPportName;
	}

	public static int getDataRate() {
		return DATA_RATE;
	}
	
}
